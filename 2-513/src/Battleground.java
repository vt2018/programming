import pokemons.*;
import ru.ifmo.se.pokemon.Battle;

public class Battleground {
    public static void main(String[] args) {
        Battle b = new Battle();

        b.addAlly(new Luvdisc("Андрей", 22));
        b.addAlly(new MimeJr("Лёва", 21));
        b.addAlly(new MrMime("Сеня", 17));

        b.addFoe(new Sewaddle("Фара", 15));
        b.addFoe(new Swadloon("Хабиб", 12));
        b.addFoe(new Leavanny("Коннор", 22));

        b.go();
    }
}