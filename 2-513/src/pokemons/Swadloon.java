package pokemons;

import ru.ifmo.se.pokemon.Effect;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.StatusMove;
import ru.ifmo.se.pokemon.Type;

public class Swadloon extends Sewaddle {
    public Swadloon(String name, int lvl) {
        super(name, lvl);
        setStats(55, 63, 90, 50, 80, 42);
        setMove(new CalmMind(), new Swagger(), new GrassWhistle());
    }
}

class GrassWhistle extends StatusMove {
    GrassWhistle() {
        super(Type.GRASS, 0, 0.55D);
    }

    @Override
    protected void applyOppEffects(Pokemon p) {
        Effect.sleep(p);
    }
}