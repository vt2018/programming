package pokemons;

import ru.ifmo.se.pokemon.*;

public class Sewaddle extends Pokemon {
    public Sewaddle(String name, int lvl) {
        super(name, lvl);
        setStats(45, 53, 70, 40, 60, 42);
        setType(Type.BUG, Type.GRASS);
        setMove(new CalmMind(), new Swagger());
    }
}

class CalmMind extends StatusMove {
    CalmMind() {
        super(Type.PSYCHIC, 0, 1.0D);
    }

    @Override
    protected void applySelfEffects(Pokemon p) {
        p.setMod(Stat.SPECIAL_ATTACK, 1);
        p.setMod(Stat.SPECIAL_DEFENSE, 1);
    }
}

class Swagger extends StatusMove {
    Swagger() {
        super(Type.NORMAL, 0, 0.85D);
    }

    protected void applyOppEffects(Pokemon p) {
        p.setMod(Stat.ATTACK, 2);
        Effect.confuse(p);
    }
}