package pokemons;

import ru.ifmo.se.pokemon.*;

public class Luvdisc extends Pokemon {
    public Luvdisc(String name, int lvl) {
        super(name, lvl);
        setStats(43, 30, 55, 30, 65, 97);
        setType(Type.WATER);
        setMove(new Supersonic(), new TakeDown(), new Splash(), new HydroPump());
    }
}

class Supersonic extends StatusMove {
    Supersonic() {
        super(Type.NORMAL, 0, 0.55D);
    }

    @Override
    protected void applyOppEffects(Pokemon p) {
        Effect.confuse(p);
    }
}

class TakeDown extends PhysicalMove {
    TakeDown() {
        super(Type.NORMAL, 90, 0.85D);
    }

    @Override
    protected void applySelfDamage(Pokemon pokemon, double v) {
        pokemon.setMod(Stat.HP, (int)Math.round(v / 4));
    }
}

class Splash extends StatusMove {
    Splash() {
        super(Type.NORMAL, 0, 1.0D);
    }
}

class HydroPump extends SpecialMove {
    HydroPump() {
        super(Type.WATER, 110, 0.80D);
    }
}