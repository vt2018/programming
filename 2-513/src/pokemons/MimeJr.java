package pokemons;

import ru.ifmo.se.pokemon.*;

public class MimeJr extends Pokemon {
    public MimeJr(String name, int lvl) {
        super(name, lvl);
        setStats(43, 30, 55, 30, 65, 97);
        setType(Type.PSYCHIC, Type.FAIRY);
        setMove(new Psychic(), new TeeterDance(), new DreamEater());
    }
}

class Psychic extends SpecialMove {
    Psychic() {
        super(Type.PSYCHIC, 90, 1.0D);
    }

    @Override
    protected void applyOppEffects(Pokemon p) {
        p.addEffect(
                new Effect().chance(0.1D).stat(Stat.SPECIAL_DEFENSE, -1)
        );
    }
}

class TeeterDance extends StatusMove {
    TeeterDance() {
        super(Type.NORMAL, 0, 1.0D);
    }

    @Override
    protected void applyOppEffects(Pokemon p) {
        Effect.confuse(p);
    }
}

class DreamEater extends SpecialMove {
    DreamEater() {
        super(Type.PSYCHIC, 100, 1.0D);
    }

    @Override
    protected void applySelfDamage(Pokemon pokemon, double v) {
        pokemon.setMod(Stat.HP, -(int) (v / 2));
    }
}