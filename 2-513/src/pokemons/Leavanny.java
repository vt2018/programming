package pokemons;

import ru.ifmo.se.pokemon.PhysicalMove;
import ru.ifmo.se.pokemon.Pokemon;
import ru.ifmo.se.pokemon.Type;

public class Leavanny extends Swadloon {
    public Leavanny(String name, int lvl) {
        super(name, lvl);
        setStats(75, 103, 80, 70, 80, 92);
        setMove(new CalmMind(), new Swagger(), new GrassWhistle(), new RazorLeaf());
    }
}

class RazorLeaf extends PhysicalMove {
    RazorLeaf() {
        super(Type.GRASS, 55, 0.95D);
    }

    protected double calcCriticalHit(Pokemon var1, Pokemon var2) {
        return 3.0D;
    }
}