package pokemons;

import ru.ifmo.se.pokemon.PhysicalMove;
import ru.ifmo.se.pokemon.Type;

public class MrMime extends MimeJr {
    public MrMime(String name, int lvl) {
        super(name, lvl);
        setStats(40, 45, 65, 100, 120, 90);
        setMove(new Psychic(), new TeeterDance(), new DreamEater(), new AerialAce());
    }
}

class AerialAce extends PhysicalMove {
    AerialAce() {
        super(Type.FLYING, 60, 1.0D);
    }
}