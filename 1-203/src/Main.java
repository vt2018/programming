package com.company;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        float[] x = new float[19];
        short[] c = {4, 6, 8, 10, 12, 14, 16, 18, 20, 22};
        final float FIRST_BORDER = -14.0f;
        final float SECOND_BORDER = 9.0f;
        double[][] k = new double[19][10];

        for (int i = 0; i < 19; i++) {
            x[i] = FIRST_BORDER + (int) (Math.random() * ((SECOND_BORDER - FIRST_BORDER) + 1));
            for (int j = 0; j < 10; j++) {
                if (c[Math.abs(i - 9)] == 18) {
                    k[i][j] = Math.pow(Math.E, 3 / (Math.sin(x[j]) + 1));
                } else {
                    if (c[Math.abs(i - 9)] >= 12 && c[Math.abs(i - 9)] % 2 == 0 && c[Math.abs(i - 9)] != 18) {
                        k[i][j] = Math.pow(
                                Math.atan(0.25 * (x[j] - 2.5) / 23) / 2,
                                Math.pow(
                                        Math.tan(x[j]),
                                        Math.pow(Math.E, x[j]) + 0.25
                                )
                        );
                    } else {
                        k[i][j] = Math.asin(Math.cos(Math.atan(Math.cos(
                                Math.pow(Math.pow(x[j] / (x[j] - 1), 3) - 1, 0.5 / x[j])))));
                    }
                }
            }
        }

        for (double p[] : k) {
            for (double y : p) {
                System.out.print(String.format("%-10.7s\t", y));
            }
            System.out.println();
        }

    }
}
