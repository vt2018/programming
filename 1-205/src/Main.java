import static java.lang.Math.*;

public class Main {

    public static void main(String[] args) {
        short l[] = new short[10];
        short counter = 0;

        float x[] = new float[17];
        float randomStart = -9.0f, randomEnd = 14.0f;                           //For random();

        double b[][] = new double[10][17];
        short arrayForCompareFunction[] = {7, 9, 17, 19, 23};

//        -------------Part 1-----------------

        for (short i = 5; i < 24; i++) {
            if ((i % 2) != 0) {
                l[counter] = i;
                counter++;
            }
        }

//        -------------Part 2-----------------

        for (int i = 0; i < 17; i++) {
            x[i] = randomStart + ((float) random() * randomEnd);
        }

//        -------------Part 3-----------------

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 17; j++) {
                if (l[i] == 15) {
                    b[i][j] = pow(log(abs(x[j])), 3);
                } else if (Comp(l[i], arrayForCompareFunction)) {
                    b[i][j] = pow((0.25f * tan(pow(((x[j] + 1) / 4), x[j]))), sin(asin((x[j] + 2.5f) / 23)));
                } else {
                    b[i][j] = asin(sin(log(pow(2 * abs(x[j]), (tan(x[j]) - 3 / 4) / tan(x[j])))));
                }
            }
        }

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 17; j++) {
                System.out.printf("%8.3f", b[i][j]);
            }
            System.out.println();
        }
    }

    public static boolean Comp(short who, short with[]) {
        for (short data : with) {
            if (who == data) {
                return true;
            }
        }
        return false;
    }
}
