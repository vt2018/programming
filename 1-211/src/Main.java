public class Main {
    private static short[] d;
    private static float[] x;
    private static float[][] n = new float[9][19];

    private static final float r_min = (float) -3.0;
    private static final float r_max = (float) 4.0;

    private static void first() {
        d = new short[21 - 5 + 1];
        for (int i = 0; i < d.length; ++i) {
            d[i] = (short) (i + 5);
        }
    }

    private static void second() {
        x = new float[19];
        for (int i = 0; i < x.length; ++i) {
            x[i] = (float) (r_min + Math.random() * (r_max - r_min));
        }
    }

    private static void third() {
        for (int i = 0; i < n.length; ++i) {
            for (int j = 0; j < n[i].length; ++j) {
                final float X = x[j];
                switch (d[i]) {
                    case 13:
                        float base = (float) Math.pow(Math.cos(X), Math.sin(X) / (Math.cbrt(X) - 0.5));
                        float rank_top = (float) (0.25 - Math.pow(3.0 / 4 * Math.pow(X / (1.0 / 3 + X), X), Math.asin((X + 0.5) / 7)));
                        float rank_bot = (float) Math.asin(Math.pow(Math.E, -Math.abs(X)));
                        n[i][j] = (float) Math.pow(base, rank_top / rank_bot);
                        break;
                    case 9:
                    case 15:
                    case 17:
                    case 21:
                        n[i][j] = (float) Math.cos(Math.pow((Math.cbrt(X) - 1) / (1 - X) / X, 3));
                        break;
                    default:
                        n[i][j] = (float) (2 * Math.atan(Math.cos(Math.cos(Math.cos(X)))));
                }
            }
        }
    }

    private static void fourth() {
        for (float[] i : n) {
            for (float j : i) {
                System.out.printf("%-13.3f", j);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        first();
        second();
        third();
        fourth();
    }
}
