public class Main {
    private static long[] m = new long[10];
    private static double[] x = new double[14];
    private static double[][] r = new double[10][14];

    private static void first() {       // \O\  /O/  \O\  /O/
        int a = 6;                      //  ▌    ▌     ▌   ▌
        for (int i = 0; i < 10; i++)    // / |  | \  / |  | \
        {                               // /O/ \O\  /O/  \O\
            m[i] = a;                   //  ▌    ▌   ▌     ▌
            a = a + 2;                  // | \ / |  | \  / |
        }
    }

    private static void second() {
        for (int i = 0; i < 14; i++) {
            x[i] = -15.0 + Math.random() * 21;
        }
    }

    private static void third() {
        for (int i = 0; i < 10; i++)        // ___  ___
        {                                   // |  |    |  /|  /|  /|
            for (int j = 0; j < 14; j++)    // |__| ___| / | / | / |
            {                               // |       |   |   |   |
                double z = x[j];            // |    ___|   |   |   |
                switch (i) {// Использую номер  расположения элемента а не элемент из-за невозможности привести long к int
                    case 5:
                        r[i][j] = Math.tan(Math.pow((Math.pow((z * (z - 1)), z) / 2), Math.pow(z, 2)));
                        break;
                    case 1:
                    case 2:
                    case 4:
                    case 8:
                        r[i][j] = 3 / (Math.tan(Math.pow(z, z / 36)) + 1);
                        break;
                    default:
                        r[i][j] = Math.cos(Math.pow(Math.abs(z), 1.0 / 9));
                }
            }
        }
    }

    private static void fourth() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 14; j++) {
                System.out.printf("%.2f ", r[i][j]);
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {    //██ ██████ ███    ███  ██████
        System.out.println("Hello pidr");       // ██    ██    ████ ████  ██   ██
        first();                                //  ██    ██    ██ ███ ██   ██   ██
        second();                               //   ██    ██    ██  ██  ██   ██   ██
        third();                                //    ██    ██    ██       ██   ██████
        fourth();
    }
}
