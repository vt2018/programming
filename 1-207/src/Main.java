public class Main {
    public static void main(String[] args) {
        long[] m = new long[18];
        for (int i = 0; i < m.length; i++) {
            m[i] = 19 - i;
        }
        float[] x = new float[20];
        for (int i = 0; i < x.length; i++) {
            x[i] = (float) (Math.random() * 8.0 - 5.0);
        }
        float[][] k = new float[18][20];
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < x.length; j++) {
                final float X = x[j];
                switch (i) {
                    case 12:
                        float p1 = (float) Math.pow(X * (X - 4), 3);
                        float p2 = (float) Math.pow(Math.E, p1);
                        k[i][j] = (float) Math.cbrt(p2);
                        break;
                    case 16:
                    case 13:
                    case 11:
                    case 9:
                    case 8:
                    case 7:
                    case 6:
                    case 2:
                        float t1 = (float) ((X - 1.0 / 3) / 0.25 * Math.PI);
                        float t2 = (float) (Math.pow(t1, 3));
                        k[i][j] = (float) Math.sin(t2);
                        break;
                    default:
                        float b1 = (float) Math.pow(X, X);
                        float verh = (float) ((Math.log(Math.abs(X)) + 1) * b1);
                        float verhsin = (float) Math.pow(Math.sin(X), verh);
                        float b2 = (float) (4 + Math.tan(X));
                        float b3 = (float) ((Math.pow(b2, 3.0 / X) - 1) * 2.0 / 3);
                        float sred = (float) (Math.pow(b3, verhsin) / Math.PI);
                        float b4 = (float) (X - 3.0 / 4);
                        float b5 = (float) (4 * (1 - Math.pow(b4, 3)));
                        float b6 = (float) (Math.pow(b5, 3));
                        k[i][j] = (float) Math.pow(b6, sred);
                }
            }
        }
        for (int i = 0; i < m.length; i++) {
            for (int j = 0; j < x.length; j++) {
                System.out.printf("%7.3f ", k[i][j]);
            }
            System.out.println();
        }
    }
}
