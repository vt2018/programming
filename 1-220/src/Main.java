public class Main {

    public static void main(String[] args) {
        int[] u = new int[10];


        for (int i = 0, num1 = 23; i < u.length; i++, num1 -= 2) {
            u[i] = num1;

        }

        float[] x = new float[11];


        for (int i = 0; i < x.length; i++) {
            x[i] = ((float) (Math.random() * (4f + 9f)) - 9f);

        }

        double[][] b = new double[10][11];


        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 11; j++) {

                if (u[i] == 7) {

                    b[i][j] = Math.asin(Math.pow((x[j] - 2.5f) / 13f, 2f));

                    System.out.println(String.format("%.5f%n  ", b[i][j]));

                } else if (u[i] == 9 || u[i] == 11 || u[i] == 15 || u[i] == 17 || u[i] == 19) {

                    double atan = Math.atan((x[j] - 2.5f) / 13f);
                    double pow = Math.cbrt(atan);
                    double tanPow = Math.pow(pow, Math.tan(x[j] - Math.PI));

                    System.out.println(String.format("%.5f%n  ", tanPow));

                } else {

                    double abs = Math.abs(x[j]);
                    double pow1 = Math.pow((-1) * (abs * (Math.PI / 2 + abs)), 2f);
                    double pow2 = Math.cbrt(pow1);
                    double exp = Math.pow(Math.E, pow2);
                    double asin = Math.asin(exp);
                    double tan = Math.tan(asin);

                    System.out.println(String.format("%.5f%n  ", tan));


                }
            }
        }

    }
}
